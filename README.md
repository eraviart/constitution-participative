# Constitution participative

Meta-projet traitant des différents logiciels utilisés pour le projet de réforme participative de la constitution

## Modules du projet

### Rédaction et dépôt d'un amendement

* Récupérer la Constitution dans le site de la Dila
* Installer un éditeur wysiwyg permettant d'éditer en wysiwyg la constitution et de générer un commit/fork/pull-request
* Générer de la légistique d'après le diff
* Envoyer l'amendement (sous forme légistique) dans Eloi
* Conserver une relation entre le commit et la référence de l'amendement.

Pour les citoyens, au lieu d'envoyer dans Eloi, générer une pull request qui peut être ensuite reprise par un député et insérée dans Eloi.

### Débat et vote d'un amendement

* Afficher la Constitution, avant et après l'amendement, le diff, l'amendement sous forme légistique et sous forme de diff
* Une fois que l'amendement est voté, modifier immédiatement la Constitution affichée pour les prochains amendements
* Afficher les arguments pour et les arguments contre chaque amendement (et permettre aux citoyens d'en ajouter, de les noter)
* Insérer les amendements et/ou les arguments à côté du direct des débats.
